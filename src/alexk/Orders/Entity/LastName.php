<?php
/**
 * Created by PhpStorm.
 * User: alexk
 * Date: 8/23/17
 * Time: 1:08 AM
 */

namespace alexk\Orders\Entity;

class LastName
{
    /** @var  string */
    private $name;

    /**
     * LastName constructor.
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}