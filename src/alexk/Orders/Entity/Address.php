<?php
/**
 * Created by PhpStorm.
 * User: alexk
 * Date: 8/23/17
 * Time: 12:40 AM
 */

namespace alexk\Orders\Entity;

class Address
{
    /** @var  string */
    private $name;
    /** @var  string */
    private $longitude;
    /** @var  string */
    private $lattitude;

    /**
     * Address constructor.
     *
     * @param string $name
     * @param string $longitude
     * @param string $latitude
     */
    public function
    __construct($name, $longitude, $latitude)
    {
        $this->name = $name;
        $this->longitude = $longitude;
        $this->lattitude = $latitude;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return string
     */
    public function getLattitude()
    {
        return $this->lattitude;
    }


}