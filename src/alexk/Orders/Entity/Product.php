<?php
/**
 * Created by PhpStorm.
 * User: alexk
 * Date: 8/23/17
 * Time: 1:04 AM
 */

namespace alexk\Orders\Entity;

class Product
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $price;

    /**
     * Product constructor.
     *
     * @param string $title
     * @param string $price
     */
    public function __construct($title, $price)
    {
        $this->title = $title;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }


}