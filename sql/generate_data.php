<?php
/**
 * Created by PhpStorm.
 * User: a.kochetov
 * Date: 22/08/17
 * Time: 19:42
 */


include dirname(__DIR__).'/src/bootstrap.php';

$addresses = [];
$addressFile = new \SplFileObject(dirname(__DIR__).'/data/citylist.csv');
$addressFile->setFlags(\SplFileObject::READ_CSV | \SplFileObject::DROP_NEW_LINE | \SplFileObject::SKIP_EMPTY);
foreach ($addressFile as $row) {
    if (!empty($row[0])) {
        array_push($addresses, new \alexk\Orders\Entity\Address($row[0], $row[2], $row[3]));
    }
}

$products = [];
$productsFile = new \SplFileObject(dirname(__DIR__).'/data/Products.csv');
$productsFile->setFlags(\SplFileObject::READ_CSV | \SplFileObject::DROP_NEW_LINE | \SplFileObject::SKIP_EMPTY);
foreach ($productsFile as $row) {
    if (!empty($row[0])) {
        array_push($products, new \alexk\Orders\Entity\Product($row[1], $row[5]));
    }
}

$lastNames = [];
$lastNamesFile = new \SplFileObject(dirname(__DIR__).'/data/Last_Names.csv');
$lastNamesFile->setFlags(\SplFileObject::READ_CSV | \SplFileObject::DROP_NEW_LINE | \SplFileObject::SKIP_EMPTY);
foreach ($lastNamesFile as $row) {
    if (!empty($row[0])) {
        array_push($lastNames, new \alexk\Orders\Entity\LastName($row[0]));
    }
}
//var_dump(sizeof($lastNames));


for ($i=0; $i<1;$i++) {

}