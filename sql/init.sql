DROP TABLE `raw_orders`;
CREATE TABLE `raw_orders` (
  `order_number` varchar(255) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `order_category` varchar(255) DEFAULT NULL,
  `product_title` varchar(255) DEFAULT NULL,
  `product_quantity` varchar(255) DEFAULT NULL,
  `product_price` varchar(255) DEFAULT NULL,
  `shipping_address` varchar(255) DEFAULT NULL,
  `shipping_coordinates` point DEFAULT NULL,
  `shipping_quadtree_index` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`shipping_quadtree_index`),
  KEY `order_number` (`order_number`),
  KEY `shipping_coordinates` (`shipping_coordinates`(25))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;